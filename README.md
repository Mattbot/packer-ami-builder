# Packer AWS EC2 AMI Builder

Build AWS EC2 AMI images via Packer and Terraform running in Docker. Terraform infrastructure state is stored in an S3 bucket.

## AMI Templates

### templates/jmeter.json
  Apache JMeter 5.1 and Java 8 on Ubuntu 18.04

## Setup

Requires Docker (to run Packer and Terraform via containers), AWS CLI, and direnv.

To setup up the AWS AMI build instructure:

```bash
  direnv allow
  cp .envrc.example .envrc
```
  Enter your AWS credentials into the ```.envrc``` file.

```bash
  setup
```

After the setup command runs, note the VPC_ID and one of the SUBNET_IDs created by Terraform and add it to the ```.envrc``` file.  Reload the ```.envrc``` file:

```bash
  direnv allow
```

## To Build an AMI from an included template:

```bash
  packer validate templates/jmeter.json
  packer build templates/jmeter.json
```
## Teardown

To tear down the AWS build infrastructure:

```bash
  teardown
```
