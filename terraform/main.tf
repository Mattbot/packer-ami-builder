terraform {
  backend "s3" {
    bucket = "packer-ami-builder-tf-state"
    key    = "packer-ami-builder.tfstate"
  }
}
resource "aws_vpc" "default" {
  cidr_block           = "${var.cidr}"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags {
    Name        = "${var.name}"
    Paradigm    = "${var.name}"
  }
}

data "aws_subnet_ids" "default" {
  vpc_id = "${aws_vpc.default.id}"
}

# Create an internet gateway to give the subnets access to the outside world
resource "aws_internet_gateway" "default" {
  vpc_id = "${aws_vpc.default.id}"

  tags {
    Paradigm    = "${var.name}"
  }
}

# Grant the VPC internet access on its main route table
resource "aws_route" "internet_access" {
  route_table_id         = "${aws_vpc.default.main_route_table_id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.default.id}"
}

data "aws_availability_zones" "available" {
  state = "available"
}

# Create a subnet to launch our instances into
resource "aws_subnet" "default" {
  count                   = "${length(slice(data.aws_availability_zones.available.names, 1, length(data.aws_availability_zones.available.names)))}"
  vpc_id                  = "${aws_vpc.default.id}"
  cidr_block              = "${cidrsubnet("${aws_vpc.default.cidr_block}", "${var.subnet_bitsize}", "${count.index}")}"
  availability_zone       = "${format("%s", element(slice(data.aws_availability_zones.available.names, 1, length(data.aws_availability_zones.available.names)), count.index))}"
  map_public_ip_on_launch = true

  tags {
    Paradigm    = "${var.name}"
  }
}
